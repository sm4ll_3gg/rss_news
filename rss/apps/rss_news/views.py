from django.http import HttpResponse
from django.shortcuts import render
from rss.apps.rss_news.models import Post

# Create your views here.


def index(request):

    posts = reversed(Post.objects.all())

    context = {
        'posts': posts
    }

    return render(request, "index.html", context)


def post(request, index):

    try:
        post = Post.objects.get(id=index)

        context = {
            "post": post
        }

    except Post.DoesNotExist:
        post = Post()
        post.id = int(index)
        post.title = ":("
        post.text = "That's a pity (and a paradox by the way), but this post does not exist!"

    return render(request, "post.html", context)

def login(request):
    return render(request, "login.html")


def signup(request):
    return render(request, "signup.html")
